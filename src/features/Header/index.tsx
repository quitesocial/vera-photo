import React from 'react'

import { HeaderStyled, HeaderLogo } from './styled'

export default () => (
  <HeaderStyled>
    <HeaderLogo to='/' aria-label='Go to main page' />
  </HeaderStyled>
)

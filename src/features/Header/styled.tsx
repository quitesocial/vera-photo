import styled from 'styled-components/macro'
import { Link } from 'react-router-dom'

export const HeaderStyled = styled.header`
  display: flex;
  width: 100%;
  height: 180px;
  justify-content: center;
  align-items: center;
  border-bottom: 1px solid #dff2c6;
  @media screen and (max-width: 700px) {
    width: 275px;
    height: auto;
    border-bottom: none;
    border-right: 1px solid #dff2c6;
  }
`

export const HeaderLogo = styled(Link)`
  width: 347px;
  height: 101px;
  background-image: url('/images/logo.png');
  background-repeat: no-repeat;
  background-size: contain;
  @media screen and (max-width: 700px) {
    width: 210px;
    height: 64px;
  }
`

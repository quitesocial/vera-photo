import styled from 'styled-components/macro'

export const FooterStyled = styled.footer`
  display: flex;
  position: absolute;
  bottom: 0;
  flex-direction: column;
  width: 100%;
  height: 130px;
  background-color: #c0e68d;
  justify-content: center;
  align-items: center;
  @media screen and (max-width: 700px) {
    height: 85px;
  }
`

export const FooterSocials = styled.ul`
  display: flex;
  align-items: center;
`

export const FooterCopyright = styled.div`
  font-weight: 600;
  font-size: 15px;
  line-height: 15px;
  margin-top: 28px;
  @media screen and (max-width: 700px) {
    margin-top: 15px;
    font-size: 12px;
    line-height: 12px;
  }
`

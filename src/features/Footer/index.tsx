import React from 'react'

import { socials } from 'config'

import SocialItem from 'features/common/SocialItem'

import {
  FooterStyled,
  FooterSocials,
  FooterCopyright,
} from './styled'

type TSocials = {
  link: string,
  name: string,
}

export default () => (
  <FooterStyled>
    <FooterSocials>
      {socials.map(({ link, name }: TSocials, idx: number) => (
        <SocialItem
          key={idx}
          name={name}
          link={link}
        />
      ))}
    </FooterSocials>
    <FooterCopyright>
      &copy; Москва, 2019
    </FooterCopyright>
  </FooterStyled>
)

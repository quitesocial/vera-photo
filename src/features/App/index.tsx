import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'

import { TopWrapper } from './styled'

const Footer = lazy(() => import('features/Footer'))
const Header = lazy(() => import('features/Header'))
const Menu = lazy(() => import('features/Main/Menu'))
const Nav = lazy(() => import('features/Nav'))

const { Fragment } = React

export default () => (
  <Fragment>
    <Suspense fallback={<div>Loading...</div>}>
      <TopWrapper>
        <Header />
        <Nav />
      </TopWrapper>
    </Suspense>
    <Suspense fallback={<div>Loading...</div>}>
      <Route exact path='/' component={() => <Menu />} />
    </Suspense>
    <Suspense fallback={<div>Loading...</div>}>
      <Footer />
    </Suspense>
  </Fragment>
)

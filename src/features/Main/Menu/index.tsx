import React, { lazy, Suspense } from 'react'
import styled from 'styled-components/macro'

import { menuItems } from 'config'

const MenuItem = lazy(() => import('./MenuItem'))

type TMenuItem = {
  link: string,
  name: string,
  image: string,
  className: string,
}

export const MenuStyled = styled.ul`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin: 0;
  padding: 0;
  list-style: none;
`

const Menu = () => (
  <MenuStyled>
    {menuItems.map(({
                      link,
                      name,
                      image,
                      className,
                    }: TMenuItem,
                    idx: number) => (
                      <Suspense
                        fallback={<div>Loading...</div>}
                        key={`${name}_${idx}`}
                      >
                       <MenuItem
                         link={link}
                         name={name}
                         image={image}
                         className={className}
                       />
                     </Suspense>
                   ),
    )}
  </MenuStyled>
)

export default Menu

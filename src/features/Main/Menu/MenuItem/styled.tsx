import styled from 'styled-components/macro'
import { Link } from 'react-router-dom'

export const MenuUnderlayer = styled.div`
  display: block;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  border-bottom: 2px solid rgba(#272727, .5);
  background-color: rgba(white, .35);
  pointer-events: none;
  @media screen and (max-width: 700px) {
    display: none;
  }
`

export const MenuLabel = styled.h2`
  margin: 0;
  font-size: 48px;
  line-height: 48px;
  font-weight: 500;
  text-transform: uppercase;
  white-space: nowrap;
  letter-spacing: 1px;
  z-index: 5;
  transition: .3s;
  @media screen and (max-width: 700px) {
    width: 100%;
    text-align: center;
    background-color: rgba(#c0e68d, .35);
    padding: 18px 0;
  }
`

export const MenuLink = styled(Link)`
  display: flex;
  position: absolute;
  height: 48px;
  left: 50%;
  bottom: 22px;
  transform: translateX(-50%);
  align-items: center;
  justify-content: center;
  text-decoration: none;
  color: black;
  background-color: #c0e68d;
  padding: 0 38px;
  border-radius: 38px;
  font-size: 18px;
  line-height: 18px;
  font-weight: 500;
  white-space: nowrap;
  opacity: 0;
  transition: .3s;
  &:hover {
    box-shadow: 0 0 18px #c0e68d;
    transition: .3s;
  }
  @media screen and (max-width: 700px) {
    opacity: 0;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    transform: none;
  }
`

export const MenuItemStyled = styled.li`
  display: flex;
  position: relative;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 314px;
  margin: 0;
  padding: 0;
  list-style: none;
  background-repeat: no-repeat;
  background-size: 100%;
  background-position: center center;
  transition: .3s;
  background-position: ${({ className }) => {
    switch (className) {
      case 'architecture':
        return `center 32%`
      case 'nature':
        return `center 71%`
      case 'animals':
        return `center 58%`
      case 'people':
        return `center 33%`
      case 'objects':
        return `center 32%`
      default:
        return ''
    }
  }};
  @media screen and (max-width: 700px) {
    height: 250px;
  }
  &:hover {
    background-size: 105%;
    transition: .3s;
    ${MenuUnderlayer} {
      opacity: 0;
      pointer-events: none;
      transition: .3s;
    }
    ${MenuLabel} {
      opacity: 0;
      pointer-events: none;
      transition: .3s;
      @media screen and (max-width: 700px) {
        opacity: 1;
      }
    }
    ${MenuLink} {
      opacity: 1;
      transition: .3s;
      @media screen and (max-width: 700px) {
        opacity: 0;
      }
    }
  }
`

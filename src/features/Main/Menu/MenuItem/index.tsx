import React from 'react'

import {
  MenuUnderlayer,
  MenuLabel,
  MenuLink,
  MenuItemStyled,
} from './styled'

type TMenuItem = {
  link: string,
  image: string,
  name: string,
  className: string,
}

const MenuItem = ({
                           link,
                           image,
                           name,
                           className,
                         }: TMenuItem) => (
  <MenuItemStyled
    className={className}
    style={{ backgroundImage: `url(${image})` }}
  >
    <MenuUnderlayer />
    <MenuLabel>{name}</MenuLabel>
    <MenuLink to={link}>посмотреть все фото</MenuLink>
  </MenuItemStyled>
)

export default MenuItem

import styled from 'styled-components/macro'

export const Social = styled.li`
  display: flex;
  width: 45px;
  height: 45px;
  justify-content: center;
  align-items: center;
  border-radius: 10px;
  background-color: #d3eeaf;
  border: 1px solid #88a967;
  margin-right: 41px;
  @media screen and (max-width: 700px) {
    width: 32px;
    height: 32px;
    margin-right: 31px;
  }
  &:last-child {
    margin-right: 0;
  }
`

export const SocialLink = styled.a`
  display: block;
  width: 80%;
  height: 80%;
  text-decoration: none;
  background-size: contain;
  background-repeat: no-repeat;
  cursor: pointer;
  opacity: .7;
  transition: .3s;
  &:hover {
    opacity: 1;
    transition: .3s;
  }
`

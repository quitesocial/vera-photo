import * as React from 'react'

import { Social, SocialLink } from './styled'

type TSocialItem = {
  /** name social net, eg Facebook, Twitter etc */
  name: string,
  /** link to the needed social */
  link: string,
}

const SocialItem = ({ name, link }: TSocialItem) =>
  <Social>
    <SocialLink
      aria-label={`Go to ${name}`}
      rel='noopener'
      target='_blank'
      href={link}
      style={{ backgroundImage: `url(images/${name}.svg)` }}
    />
  </Social>

export default SocialItem

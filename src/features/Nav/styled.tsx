import styled from 'styled-components/macro'
import { Link } from 'react-router-dom'

export const NavStyled = styled.nav`
  display: flex;
  width: 100%;
  height: 133px;
  justify-content: center;
  align-items: center;
  @media screen and (max-width: 1045px) {
    padding: 0 60px;
  }
  @media screen and (max-width: 700px) {
    width: auto;
    height: auto;
    flex: 1;
    padding: 0 30px;
  }
`

export const NavContainer = styled.div`
  display: flex;
  width: 1128px;
  justify-content: space-between;
  align-items: center;
  @media screen and (max-width: 1045px) {
    width: 100%;
  }
`

export const NavLink = styled(Link)`
  text-decoration: none;
  font-size: 24px;
  line-height: 24px;
  color: #626263;
  text-transform: uppercase;
  transition: .1s;
  @media screen and (max-width: 1045px) {
    font-weight: 500;
  }
  @media screen and (max-width: 700px) {
    font-size: 16px;
    line-height: 16px;
  }
  &:hover {
    color: #c0e68d;
    transition: .1s;
  }
`

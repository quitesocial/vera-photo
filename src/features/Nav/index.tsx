import React from 'react'

import {
  NavStyled,
  NavContainer,
  NavLink,
} from './styled'

export default () => (
  <NavStyled>
    <NavContainer>
      <NavLink to='/about'>обо мне</NavLink>
      <NavLink to='/portfolio'>портфолио</NavLink>
      <NavLink to='/contacts'>контакты</NavLink>
    </NavContainer>
  </NavStyled>
)

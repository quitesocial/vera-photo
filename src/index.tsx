import React, { lazy, Suspense } from 'react'
import reactDom from 'react-dom'
import { BrowserRouter } from 'react-router-dom'

const App = lazy(() => import('features/App'))

reactDom.render(
  <BrowserRouter>
    <Suspense fallback={<div>Loading...</div>}>
      <App />
    </Suspense>
  </BrowserRouter>,
  document.getElementById('root'),
)

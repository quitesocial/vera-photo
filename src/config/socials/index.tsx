type TSocials = {
  name: string,
  link: string,
}

export const socials: TSocials[] = [
  {
    name: 'instagram',
    link: 'https://www.instagram.com/verarazdorskaya/',
  },
  {
    name: 'vkontakte',
    link: 'https://vk.com/razdorskaya_vera',
  },
  {
    name: 'pinterest',
    link: 'https://pinterest.com',
  },
  {
    name: 'facebook',
    link: 'https://www.facebook.com/razdorskayavera',
  },
]

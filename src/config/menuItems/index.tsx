type TMenuItems = {
  name: string,
  link: string,
  image: string,
  className: string,
}

export const menuItems: TMenuItems[] = [
  {
    name: 'архитектура',
    link: '/portfolio/architecture',
    image: '/images/architecture.jpg',
    className: 'architecture',
  },
  {
    name: 'природа',
    link: '/portfolio/nature',
    image: '/images/nature.jpg',
    className: 'nature',
  },
  {
    name: 'животные',
    link: '/portfolio/animals',
    image: '/images/animals.jpg',
    className: 'animals',
  },
  {
    name: 'люди',
    link: '/portfolio/people',
    image: '/images/people.jpg',
    className: 'people',
  },
  {
    name: 'предметы',
    link: '/portfolio/objects',
    image: '/images/objects.jpg',
    className: 'objects',
  },
]

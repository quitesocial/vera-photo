const path = require('path')

module.exports = {
  components: 'src/features/**/*.tsx',
  moduleAliases: {
    config: path.resolve(__dirname, 'src/config'),
    features: path.resolve(__dirname, 'src/features'),
  },
}

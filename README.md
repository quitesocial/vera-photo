Vera Razdorskaya - photography

v.0.1.0

___

v0.1.1

- updated packages
- added eslint TS support
- added modules resolution (aliases resolving) to `styleguidist` config file
- remove stupid snapshots and tests
- changed all files' extensions to tsx
